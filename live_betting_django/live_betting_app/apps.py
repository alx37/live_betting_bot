from django.apps import AppConfig


class LiveBettingConfig(AppConfig):
    name = 'live_betting'
