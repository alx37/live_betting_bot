from django.db import models


class Match(models.Model):
    match_site = models.CharField(max_length=127)
    match_id = models.CharField(max_length=127, unique=True)
    date = models.DateField()
    team_left_name = models.CharField(max_length=200)
    team_right_name = models.CharField(max_length=200)
    bet_coef_right = models.DecimalField(decimal_places=2, max_digits=4, null=True, blank=True)
    bet_coef_left = models.DecimalField(decimal_places=2, max_digits=4, null=True, blank=True)
    bet_amount_right = models.DecimalField(decimal_places=2, max_digits=6, null=True, blank=True)
    bet_amount_left = models.DecimalField(decimal_places=2, max_digits=6, null=True, blank=True)
    coef_history = models.CharField(max_length=50000, default="")
    winner = models.CharField(max_length=100, null=True, blank=True)
