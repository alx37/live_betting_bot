import scrapy
from live_betting_app.models import Match
from decimal import Decimal
import json

class LootBetSpider(scrapy.Spider):
    name = "lootbet"

    def start_requests(self):
        urls = [
           "https://egb.com/play/live"
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        matches = response.selector.css(".match-container")
        for match_elmt in matches:
            match_id = match_elmt.css(".itemNew").xpath("@id").extract()
            team_left_coef = Decimal(match_elmt.css(".itemNew .teamLeft").xpath('//span[@class="cof"]/text()').extract_first().strip())
            team_right_coef = Decimal(match_elmt.css(".itemNew .teamright").xpath('//span[@class="cof"]/text()').extract_first().strip())
            new_history_element = {
                "timestamp": timezone.now().isoformat(),
                "team_left_coef": team_left_coef,
                "team_right_coef": team_right_coef
            }
            try:
                match = Match.objects.get(match_id=match_id)
                if match.team_left_coef != team_left_coef or match.team_right_coef != team_right_coef:
                    history = json.load(match.history)
                    history.append(new_history_element)
                    match.update(coef_history=json.dumps(history))
            except:
                match = Match.object.create(
                    match_id=match_id,
                    team_left_name=left_name,
                    team_right_name=right_name,
                    coef_history=json.dumps([new_history_element])
                )
            
        calculate_betting(match, team_left_coef, team_right_coef)