

def calculate_betting(match, team_left_coef, team_right_coef):

    # Calculate betting
    # No bets placed
    if not match.bet_coef_left and not match.bet_coef_rigth:
        if team_left_coef > 2.1 and team_left_coef < 2.8:
            match.bet_coef_left = team_left_coef
            match.bet_amount_left = 100
        elif team_right_coef > 2.1 and team_right_coef < 2.8:
            match.bet_coef_right = team_right_coef
            match.bet_amount_right = 100

    # Have not betted Left
    elif not match.bet_coef_left:
        if team_left_coef > 2.2 and team_left_coef < 2.8:
            match.bet_coef_left = team_left_coef
            match.bet_amount_left = 100

    # Have not betted right
    elif not match.bet_coef_rigth:
        if team_right_coef > 2.2 and team_right_coef < 2.8:
            match.bet_coef_right = team_right_coef
            match.bet_amount_right = 100
        
    match.save()