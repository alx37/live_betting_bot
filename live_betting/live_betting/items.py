# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Match(scrapy.Item):
    match_id = scrapy.Field()
    date = scrapy.Field()
    team_left_name = scrapy.Field()
    team_right_name = scrapy.Field()
    team_left_cof = scrapy.Field()
    team_right_cof = scrapy.Field()
